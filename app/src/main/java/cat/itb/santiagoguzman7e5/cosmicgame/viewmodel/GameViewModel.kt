package cat.itb.santiagoguzman7e5.cosmicgame.viewmodel

import android.app.Application
import android.content.res.Resources
import android.graphics.Path
import android.graphics.PointF
import android.graphics.drawable.Drawable
import android.media.MediaPlayer
import androidx.annotation.DrawableRes
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.AndroidViewModel
import cat.itb.santiagoguzman7e5.cosmicgame.R
import cat.itb.santiagoguzman7e5.cosmicgame.model.Booster
import cat.itb.santiagoguzman7e5.cosmicgame.model.GameState
import cat.itb.santiagoguzman7e5.cosmicgame.model.Joystick
import cat.itb.santiagoguzman7e5.cosmicgame.model.Level
import cat.itb.santiagoguzman7e5.cosmicgame.model.Enemy
import cat.itb.santiagoguzman7e5.cosmicgame.model.Ufo
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.flow
import kotlin.math.cos
import kotlin.math.sin
import kotlin.random.Random

class GameViewModel(private val application: Application): AndroidViewModel(application) {

    private val resources: Resources get() = application.applicationContext.resources
    private fun drawable(@DrawableRes id: Int): Drawable = ResourcesCompat.getDrawable(resources,id,null)!!

    val gameLoop = flow<GameState> {
        while (_ufo.isAlive){
            emit(GameState.RUNNING)
            delay(33)
        }
        emit(GameState.STOPPED)
    }

    private var _level: Level = Level.MeteorShower

    fun setLevel(level: Level){
        _level = level
    }

    private val _ufo: Ufo = Ufo(
        drawable(R.drawable.ic_launcher_foreground),
        PointF(
            resources.displayMetrics.widthPixels / 2f,
            resources.displayMetrics.heightPixels / 2f
        )
    )
    val ufo: Ufo
        get() = _ufo

    fun updatePositionToOvni(width: Float, height: Float) {
        _ufo.position.x = (_ufo.position.x + joystick.acceleration * cos(joystick.angle)).coerceIn(0f, width)
        _ufo.position.y = (_ufo.position.y + joystick.acceleration * sin(joystick.angle)).coerceIn(0f, height)
    }

    private var _joystick: Joystick? = null
    val joystick: Joystick
        get() = _joystick!!

    fun initJoystick(position: PointF){
        _joystick = Joystick(position,0f)
    }
    fun destroyJoystick(){
        _joystick = null
    }

    private var _enemies: MutableList<Enemy> = mutableListOf()
    val enemies: List<Enemy>
        get() = _enemies

    private var _booster: Booster? = null
    val booster: Booster?
        get() = _booster

    fun updateBooster(width: Float, height: Float){
        if (_booster == null && Random.nextInt(0, 100) < 1){
            _booster = Booster(
                drawable(R.drawable.ic_time),
                PointF(
                    (0..width.toInt()).random().toFloat(),
                    (0..height.toInt()).random().toFloat()
                )
            )
        } else if (Path().apply { _booster?.hitbox?.let { op(ufo.hitbox, it, Path.Op.INTERSECT) } }.isEmpty.not()){
            _score += 5000
            _booster = null
        } else if (_booster?.isAlive == false){
            _booster = null
        }
    }

    private fun createEnemy(width: Float, height: Float) {
        val startX = if (Random.nextBoolean()) 0f else width
        val startY = Random.nextFloat() * height
        val endX = if (startX == 0f) width else 0f
        val endY = Random.nextFloat() * height
        val direction = PointF(endX - startX, endY - startY)
        val enemy = Enemy(drawable(_level.enemyId), PointF(startX, startY), direction, Random.nextInt(50,200), _level.speed)

        // Add the meteor to the list
        _enemies.add(enemy)
    }

    fun updateEnemies(width: Float, height: Float) {
        _enemies = _enemies.filter { enemy ->
            with(enemy.position) {
                x in -enemy.radius..width + enemy.radius &&
                y in -enemy.radius..height + enemy.radius &&
                enemy.isAlive
            }
        }.map { meteor ->
            meteor.position.x += meteor.acceleration.x
            meteor.position.y += meteor.acceleration.y
            if (Path().apply { op(ufo.hitbox, meteor.hitbox, Path.Op.INTERSECT) }.isEmpty.not()){
                ufo.isAlive = false
                meteor.isAlive = false
            }
            meteor
        }.toMutableList()
        if (_enemies.size < _level.enemies && Random.nextInt(0, 100) < 25  ) createEnemy(width, height)
    }

    fun reset(){
        _ufo.drawable = drawable(R.drawable.ic_launcher_foreground)
        _ufo.position = PointF(
            resources.displayMetrics.widthPixels / 2f,
            resources.displayMetrics.heightPixels / 2f
        )
        _ufo.isAlive = true
        _enemies.removeAll { true }
        _score = 0
    }

    fun explosion() {
        MediaPlayer.create(application.applicationContext, R.raw.impact).start()
        _ufo.drawable = drawable(R.drawable.ic_explosion)
    }

    private var _score: Long = 0
    val score: String
        get() {
            val seconds = (_score / 1000).toInt()
            val millis = (_score % 1000) / 10
            return "${String.format("%02d", seconds)}:${String.format("%02d", millis)}"
        }
    fun addScore() {
        _score += 100
    }

    private val _sound: MutableStateFlow<Boolean> = MutableStateFlow(true)
    val sound: StateFlow<Boolean>
        get() = _sound

    fun toggleSound() {
        _sound.value = _sound.value.not()
    }

    private val _vibration: MutableStateFlow<Boolean> = MutableStateFlow(true)
    val vibration: StateFlow<Boolean>
        get() = _vibration

    fun toggleVibration() {
        _vibration.value = _vibration.value.not()
    }

}
