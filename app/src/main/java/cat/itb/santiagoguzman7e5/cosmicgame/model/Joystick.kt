package cat.itb.santiagoguzman7e5.cosmicgame.model

import android.graphics.PointF
import kotlin.math.atan2
import kotlin.math.pow
import kotlin.math.sqrt

class Joystick(
    override var position: PointF,
    var angle: Float
): GameObject {
    private var distance = 0f
    fun update(position: PointF) {
        val dx = position.x - this.position.x
        val dy = position.y - this.position.y
        distance = sqrt(dx.pow(2) + dy.pow(2))
        angle = atan2(dy, dx)
        this.position = position
    }

    val acceleration
        get() = distance * 5
}
