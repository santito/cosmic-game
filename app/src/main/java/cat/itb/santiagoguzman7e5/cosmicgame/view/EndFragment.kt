package cat.itb.santiagoguzman7e5.cosmicgame.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import cat.itb.santiagoguzman7e5.cosmicgame.R
import cat.itb.santiagoguzman7e5.cosmicgame.databinding.FragmentEndBinding
import cat.itb.santiagoguzman7e5.cosmicgame.viewmodel.GameViewModel


class EndFragment : Fragment() {

    private var _binding: FragmentEndBinding? = null
    private val binding get() = _binding!!
    private val viewModel: GameViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val fragmentBinding = FragmentEndBinding.inflate(inflater, container, false)
        _binding = fragmentBinding
        return fragmentBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(binding.reset){
            setOnClickListener {
                viewModel.reset()
                findNavController().navigate(R.id.action_end_to_game)
            }
        }
        with(binding.menu){
            setOnClickListener {
                viewModel.reset()
                findNavController().navigate(R.id.action_end_to_start)
            }
        }
        with(binding.score){
            text = viewModel.score
        }
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
