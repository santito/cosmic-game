package cat.itb.santiagoguzman7e5.cosmicgame.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import cat.itb.santiagoguzman7e5.cosmicgame.R
import cat.itb.santiagoguzman7e5.cosmicgame.databinding.FragmentStartBinding
import cat.itb.santiagoguzman7e5.cosmicgame.view.adapter.LevelAdapter
import cat.itb.santiagoguzman7e5.cosmicgame.viewmodel.GameViewModel
import com.google.android.material.appbar.MaterialToolbar
import com.google.android.material.carousel.CarouselLayoutManager
import kotlinx.coroutines.launch

class StartFragment : Fragment() {

    private var _binding: FragmentStartBinding? = null
    private val binding get() = _binding!!

    private val viewModel: GameViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val fragmentBinding = FragmentStartBinding.inflate(inflater, container, false)
        _binding = fragmentBinding
        return fragmentBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val startButton = binding.startButton
        startButton.setOnClickListener {
            findNavController().navigate(R.id.action_start_to_game)
        }
        with(binding.levelSelector){
            layoutManager = CarouselLayoutManager()
            adapter = LevelAdapter { level, pos ->
                smoothScrollToPosition(pos)
                viewModel.setLevel(level)
            }
        }
        setupSettings(binding.settingsBar)
    }

    private fun setupSettings(settingsBar: MaterialToolbar) = settingsBar
        .apply {
            setupSoundCollector(menu.getItem(0))
            setupVibrateCollector(menu.getItem(1))
        }

    private fun setupSoundCollector(menuItem: MenuItem) = lifecycleScope.launch {
        menuItem.setOnMenuItemClickListener {
            viewModel.toggleSound()
            viewModel.sound.value
        }
        viewModel.sound.collect { soundState ->
            if (soundState) menuItem.setIcon(R.drawable.ic_sound_on)
            else menuItem.setIcon(R.drawable.ic_sound_off)
        }
    }

    private fun setupVibrateCollector(menuItem: MenuItem) = lifecycleScope.launch {
        menuItem.setOnMenuItemClickListener {
            viewModel.toggleVibration()
            viewModel.vibration.value
        }
        viewModel.vibration.collect { vibrationState ->
            if (vibrationState) menuItem.setIcon(R.drawable.ic_vibration_on)
            else menuItem.setIcon(R.drawable.ic_vibration_off)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
