package cat.itb.santiagoguzman7e5.cosmicgame.model

import android.graphics.Bitmap
import android.graphics.Path
import android.graphics.PointF
import kotlin.math.min

interface GameObject {
    val position: PointF

    interface Collider : GameObject {
        val isAlive: Boolean

        val bitmap: Bitmap

        val radius: Float
            get() = min(bitmap.width, bitmap.height) / 2f

        val hitbox: Path
            get() = Path().apply {
                addCircle(position.x, position.y, radius / 3f, Path.Direction.CW)
            }
    }
}