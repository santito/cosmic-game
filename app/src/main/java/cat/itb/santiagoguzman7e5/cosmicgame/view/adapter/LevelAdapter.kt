package cat.itb.santiagoguzman7e5.cosmicgame.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import cat.itb.santiagoguzman7e5.cosmicgame.databinding.ItemLevelBinding
import cat.itb.santiagoguzman7e5.cosmicgame.model.Level

class LevelAdapter(private val levels: Array<Level> = Level.values(), private val onLevelSelectedListener: (Level, Int) -> Unit) :
    RecyclerView.Adapter<LevelAdapter.LevelViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LevelViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemLevelBinding.inflate(inflater, parent, false)
        return LevelViewHolder(binding)
    }

    override fun onBindViewHolder(holder: LevelViewHolder, position: Int) {
        val level = levels[position]
        holder.bind(level)
    }

    override fun getItemCount() = levels.size

    inner class LevelViewHolder(private val binding: ItemLevelBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(level: Level) {
            binding.root.setOnClickListener{
                onLevelSelectedListener(level, adapterPosition)
            }
            binding.levelIcon.setImageResource(level.id)
            binding.levelName.text = level.toString()
            binding.levelContainer.setOnMaskChangedListener { maskRect ->
                binding.levelContainer.translationX = maskRect.top
                binding.levelContainer.alpha = 1F - (maskRect.left / 500F)
            }
        }
    }

}
