package cat.itb.santiagoguzman7e5.cosmicgame.view

import android.media.MediaPlayer
import android.os.Bundle
import android.os.VibrationEffect
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.lifecycle.lifecycleScope
import cat.itb.santiagoguzman7e5.cosmicgame.R
import cat.itb.santiagoguzman7e5.cosmicgame.databinding.ActivityGameBinding
import cat.itb.santiagoguzman7e5.cosmicgame.viewmodel.GameViewModel
import kotlinx.coroutines.launch

class GameActivity : AppCompatActivity() {

    private val viewModel: GameViewModel by viewModels()
    private lateinit var binding: ActivityGameBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        installSplashScreen()
        binding = ActivityGameBinding.inflate(layoutInflater)
        setSoundManager()
        setVibrationManager()
        setContentView(binding.root)
    }

    private fun setSoundManager() = lifecycleScope.launch {
        val mediaPlayer = MediaPlayer.create(applicationContext, R.raw.music).apply {
            isLooping = true
        }
        var length = 0
        viewModel.sound.collect { soundState ->
            if (soundState) mediaPlayer.let {
                it.seekTo(length)
                it.start()
            } else {
                mediaPlayer.pause()
                length = mediaPlayer.currentPosition
            }
        }
    }

    private fun setVibrationManager() = lifecycleScope.launch {
        viewModel.vibration.collect { vibrationState ->
            val vibration = if (vibrationState) getSystemService(android.os.Vibrator::class.java).also {
                it.vibrate(VibrationEffect.createPredefined(VibrationEffect.EFFECT_CLICK))
            } else null
        }
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        if (hasFocus) {
            val decorView = window.decorView
            decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    or View.SYSTEM_UI_FLAG_FULLSCREEN)

        }
    }

    override fun onBackPressed() {
        //DISABLED
    }
}