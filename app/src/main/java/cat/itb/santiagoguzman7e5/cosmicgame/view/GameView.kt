package cat.itb.santiagoguzman7e5.cosmicgame.view

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.PointF
import android.view.MotionEvent
import android.view.SurfaceView
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import cat.itb.santiagoguzman7e5.cosmicgame.R
import cat.itb.santiagoguzman7e5.cosmicgame.model.GameState
import cat.itb.santiagoguzman7e5.cosmicgame.viewmodel.GameViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class GameView(context: Context): SurfaceView(context) {
    private lateinit var viewModel: GameViewModel
    private lateinit var lifecycleOwner: LifecycleOwner
    constructor(context: Context, gameViewModel: GameViewModel) : this(context) {
        viewModel = gameViewModel
        lifecycleOwner = context as LifecycleOwner
        lifecycleOwner.lifecycleScope.launch {
            viewModel.gameLoop.collect { state ->
                when(state) {
                    GameState.RUNNING -> invalidate()
                    GameState.STOPPED -> {
                        viewModel.explosion()
                        invalidate()
                        delay(1000)
                        findNavController().navigate(R.id.action_game_to_end)
                    }
                }
            }
        }

    }


    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        viewModel.addScore()
        viewModel.updateEnemies(width.toFloat(),height.toFloat())
        viewModel.updateBooster(width.toFloat(),height.toFloat() )
        with(viewModel.ufo){
            canvas.drawBitmap(bitmap, position.x - radius, position.y - radius, null)
        }
        with(viewModel.enemies){
            forEach { meteor ->
                canvas.drawBitmap(meteor.bitmap, meteor.position.x - meteor.radius, meteor.position.y - meteor.radius, null)
            }
        }

        with(viewModel.booster){
            if (this != null) canvas.drawBitmap(bitmap, position.x - radius, position.y - radius, null)
        }

        with(viewModel.score){
            val paint = Paint().apply {
                textSize = 50f
                typeface = context.resources.getFont(R.font.space)
                color = context.getColor(R.color.md_theme_dark_onBackground)
            }
            canvas.drawText(this, 50f, 100f, paint)
        }
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when (event?.action){
            MotionEvent.ACTION_DOWN -> {
                viewModel.initJoystick(PointF(event.x, event.y))
            }
            MotionEvent.ACTION_MOVE -> {
                viewModel.joystick.update(PointF(event.x, event.y))
                performClick()
            }
            MotionEvent.ACTION_UP -> {
                viewModel.destroyJoystick()
            }
        }
        return true
    }

    override fun performClick(): Boolean {
        viewModel.updatePositionToOvni(width.toFloat(),height.toFloat())
        return super.performClick()
    }

    init {
        setBackgroundColor(context.getColor(R.color.ic_launcher_background))
    }
}
