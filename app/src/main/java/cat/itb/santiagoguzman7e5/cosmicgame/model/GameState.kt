package cat.itb.santiagoguzman7e5.cosmicgame.model

enum class GameState {
    RUNNING, STOPPED
}