package cat.itb.santiagoguzman7e5.cosmicgame.model

import android.graphics.Bitmap
import android.graphics.Matrix
import android.graphics.PointF
import android.graphics.drawable.Drawable
import androidx.core.graphics.drawable.toBitmap
import kotlin.math.atan2
import kotlin.random.Random

data class Enemy(
    val drawable: Drawable,
    override var position: PointF,
    val direction: PointF,
    val size: Int,
    val factor: Float
): GameObject.Collider {
    override var isAlive: Boolean = true
    override val bitmap: Bitmap = drawable.toBitmap(size, size).rotateBitmap( Math.toDegrees(atan2(-direction.x, direction.y).toDouble()).toFloat() - 45)
    private fun Bitmap.rotateBitmap(angle: Float): Bitmap {
        val matrix = Matrix()
        matrix.postRotate(angle)
        return Bitmap.createBitmap(this, 0, 0, width, height, matrix, true)
    }

    val acceleration: PointF = (Random.nextFloat() * (1 / (factor * 5f) - 1 / factor) + 1 / factor)
        .let { PointF(direction.x / it, direction.y / it) }

}