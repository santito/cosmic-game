package cat.itb.santiagoguzman7e5.cosmicgame.model

import android.graphics.Bitmap
import android.graphics.PointF
import android.graphics.drawable.Drawable
import androidx.core.graphics.drawable.toBitmap

data class Ufo(
    var drawable: Drawable,
    override var position: PointF,
    override var isAlive: Boolean = true
): GameObject.Collider {
    override val bitmap: Bitmap
        get() = drawable.toBitmap()

}