package cat.itb.santiagoguzman7e5.cosmicgame.view

import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.activityViewModels
import cat.itb.santiagoguzman7e5.cosmicgame.R
import cat.itb.santiagoguzman7e5.cosmicgame.viewmodel.GameViewModel

class GameFragment : Fragment() {

    private val viewModel: GameViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): GameView? {
        return context?.let { context ->
            GameView(context, viewModel)
        }
    }

}
