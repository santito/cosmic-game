package cat.itb.santiagoguzman7e5.cosmicgame.model

import android.graphics.Bitmap
import android.graphics.PointF
import android.graphics.drawable.Drawable
import androidx.core.graphics.drawable.toBitmap

data class Booster(
    val drawable: Drawable,
    override var position: PointF,
    val durationMillis: Long = 5000
): GameObject.Collider{
    override val bitmap: Bitmap
        get() = drawable.toBitmap()


    private var startTime = System.currentTimeMillis()
    val timeLeft: Long
        get() = durationMillis - (System.currentTimeMillis() - startTime)

    override val isAlive: Boolean
        get() = timeLeft > 0
}