package cat.itb.santiagoguzman7e5.cosmicgame.model

import androidx.annotation.DrawableRes
import cat.itb.santiagoguzman7e5.cosmicgame.R

enum class Level(@DrawableRes val id: Int, val enemies: Int, val speed: Float, @DrawableRes val enemyId: Int) {
    MeteorShower(R.drawable.ic_meteor_shower,7, 0.017f, R.drawable.ic_meteor),
    BlackHole(R.drawable.ic_black_hole,5, 0.025f, R.drawable.ic_rock),
    CosmicRadiation(R.drawable.ic_cosmic_radiation,10, 0.01f, R.drawable.ic_star);

    override fun toString(): String {
        return name.replace(Regex("(\\p{Ll})(\\p{Lu})"), "$1 $2")
    }
}
